
using DownloadProgressBar.CustomControls;
using DownloadProgressBar.iOS.Renderers;
using Foundation;
using PdfKit;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof(CustomWebView), typeof(CustomWebViewRenderer))]
namespace DownloadProgressBar.iOS.Renderers
{
    public class CustomWebViewRenderer: ViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);
        
            if (e.NewElement == null)
            {
                return;
            }
        
            if (e.NewElement is CustomWebView customWebView)
            {
                var pdfView = new PdfView();
                pdfView.AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
                pdfView.AutoScales = true;
                
                var resourceUrl = new PdfDocument(new NSUrl($"file://{customWebView.Uri}"));
                
                AddSubview(pdfView);
                
                pdfView.Document = resourceUrl;
                
            }
        }
  
    }
}