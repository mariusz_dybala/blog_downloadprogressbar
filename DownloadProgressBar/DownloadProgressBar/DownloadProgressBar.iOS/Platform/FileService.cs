using System;
using System.IO;
using System.Threading.Tasks;
using DownloadProgressBar.Services.Interfaces;

namespace DownloadProgressBar.iOS.Platform
{
    public class FileService : IFileService
    {
        public async Task<string> SaveFileAsync(string fileName, Stream content)
        {
            var folderForFile = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

            var path = Path.Combine(folderForFile, fileName);
            
            var fileExists =  await Task.Run(() => File.Exists(path));

            using (var fileStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
            {
                await content.CopyToAsync(fileStream);
            }
            return path;
        }
    }
}