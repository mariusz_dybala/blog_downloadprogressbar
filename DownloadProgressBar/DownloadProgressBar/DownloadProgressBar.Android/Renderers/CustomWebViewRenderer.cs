using System.ComponentModel;
using System.IO;
using Android.Content;
using Android.Widget;
using AndroidX.Core.Content;
using DownloadProgressBar.Android.Renderers;
using DownloadProgressBar.CustomControls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomWebView), typeof(CustomWebViewRenderer))]

namespace DownloadProgressBar.Android.Renderers
{
    public class CustomWebViewRenderer : WebViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement == null)
            {
                return;
            }

            if (Element is CustomWebView customWebView)
            {
                OpenPdf(customWebView.Uri);
            }
        }

        private void OpenPdf(string pdfPath)
        {
            var context = global::Android.App.Application.Context;

            try
            {
                var fileDirectory = Path.GetDirectoryName(pdfPath);
                var fileName = Path.GetFileName(pdfPath);
                var fullFilePath = new Java.IO.File(fileDirectory, fileName);
                
                var uri = FileProvider.GetUriForFile(context, context.PackageName + ".fileProvider", fullFilePath);

                Intent intent = new Intent(Intent.ActionView);
                intent.SetDataAndType(uri, "application/pdf");
                intent.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask | ActivityFlags.GrantReadUriPermission);

                context.StartActivity(intent);
            }
            catch
            {
                Toast.MakeText(context, "No Application Available to View PDF", ToastLength.Short).Show();
            }
        }
    }
}