﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using DownloadProgressBar.Android.Platform;
using DownloadProgressBar.Services.Interfaces;

namespace DownloadProgressBar.Android
{
    [Activity(Label = "DownloadProgressBar", Theme = "@style/MainTheme", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            
            FreshMvvm.FreshIOC.Container.Register<IFileService, FileService>().AsSingleton();

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());
        }
    }
}