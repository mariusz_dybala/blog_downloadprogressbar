using System;
using System.Windows.Input;
using FreshMvvm;
using PropertyChanged;
using Xamarin.Forms;

namespace DownloadProgressBar.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class PdfViewerPageModel : FreshBasePageModel
    {
        private string _pdfPath;
        public string PdfPath
        {
            get => _pdfPath;
            set => _pdfPath = value;
        }

        public ICommand ClosePdf => new Command(() => CoreMethods.PopPageModel(true, true));
        
        public override void Init(object initData)
        {
            base.Init(initData);
        
            if (initData is string path)
            {
                PdfPath = path;
            }
        }
    }
}