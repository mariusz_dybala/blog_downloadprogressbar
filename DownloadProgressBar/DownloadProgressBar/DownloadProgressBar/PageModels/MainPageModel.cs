using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using DownloadProgressBar.Services.Interfaces;
using DownloadProgressBar.Utils;
using FreshMvvm;
using Xamarin.Forms;
using PropertyChanged;
using Xamarin.Essentials;

namespace DownloadProgressBar.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class MainPageModel : FreshBasePageModel
    {
        private const string FileToDownloadUrl = "https://pdfobject.com/pdf/sample.pdf";
        private readonly IFileDownloader _fileDownloader;
        private readonly IFileService _fileService;
        private CancellationTokenSource _cancellationTokenSource;

        private int _progress;

        public int Progress
        {
            get => _progress;
            set
            {
                _progress = value;
                ProgressDecimal = Decimal.Divide(_progress, 100);
            }
        }

        private decimal _progressDecimal;

        public decimal ProgressDecimal
        {
            get => _progressDecimal;
            set => _progressDecimal = value;
        }

        private bool _showOpenButton;

        public bool ShowOpenButton
        {
            get => _showOpenButton;
            set => _showOpenButton = value;
        }

        public ICommand DownloadFileCommand => new Command(async () => await OnDownloadFile());
        public ICommand CancelFileCommand => new Command(OnDownloadCancel);
        public ICommand OpenFileCommand => new Command(async () => await OnOpenFileCommand());

        private string _testPath = string.Empty;

        private async Task OnOpenFileCommand()
        {
            await CoreMethods.PushPageModel<PdfViewerPageModel>(_testPath, true);
        }

        private async Task OnDownloadFile()
        {
            using (var fileStream = await _fileDownloader.DownloadFileAsync(FileToDownloadUrl, OnProgress, _cancellationTokenSource.Token))
            {
                if (fileStream == null)
                {
                    return;
                }
            
                _testPath = await _fileService.SaveFileAsync("document.pdf", fileStream);
            }
        }

        private void OnDownloadCancel()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public MainPageModel(IFileDownloader fileDownloader, IFileService fileService)
        {
            _fileDownloader = fileDownloader;
            _fileService = fileService;
            _cancellationTokenSource = new CancellationTokenSource();

            Progress = 0;
        }

        private void OnProgress(FileProgress progress)
        {
            Progress = progress.Percent;

            if (progress.IsFinished)
            {
                ShowOpenButton = true;
            }
        }
    }
}