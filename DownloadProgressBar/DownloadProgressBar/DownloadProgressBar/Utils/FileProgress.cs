namespace DownloadProgressBar.Utils
{
    public class FileProgress
    {
        public bool IsFinished { get; set; }
        public int TotalBytes { get; private set; }
        public int ReadBytes { get; set; }
        public int Percent => CalculateProgress();

        public FileProgress(int totalBytes)
        {
            TotalBytes = totalBytes;
        }
        
        public FileProgress Cancel()
        {
            ReadBytes = 0;
            return this;
        }

        private int CalculateProgress()
        {
            var progress = ((float)ReadBytes / TotalBytes) * 100;

            var intValue = (int)progress;
            return intValue;
        }
    }
}