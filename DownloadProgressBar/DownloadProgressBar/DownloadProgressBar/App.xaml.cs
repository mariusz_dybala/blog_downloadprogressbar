﻿using System;
using DownloadProgressBar.PageModels;
using DownloadProgressBar.Pages;
using DownloadProgressBar.Services;
using DownloadProgressBar.Services.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using FreshMvvm;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace DownloadProgressBar
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            FreshIOC.Container.Register<IFileDownloader, FileDownloader>().AsSingleton();

            var mainPage = FreshPageModelResolver.ResolvePageModel<MainPageModel>();

            MainPage = new FreshNavigationContainer(mainPage);

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}