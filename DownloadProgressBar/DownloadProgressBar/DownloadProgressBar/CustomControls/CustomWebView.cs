using Xamarin.Forms;

namespace DownloadProgressBar.CustomControls
{
    public class CustomWebView : WebView
    {
        public static readonly BindableProperty UriProperty =
            BindableProperty.Create(nameof(Uri), typeof(string), typeof(CustomWebView));

        public string Uri
        {
            get => (string) GetValue(UriProperty);
            set => SetValue(UriProperty, value);
        }
    }
}