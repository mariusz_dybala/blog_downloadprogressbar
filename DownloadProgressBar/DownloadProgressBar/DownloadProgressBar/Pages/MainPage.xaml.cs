using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DownloadProgressBar.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
    }
}