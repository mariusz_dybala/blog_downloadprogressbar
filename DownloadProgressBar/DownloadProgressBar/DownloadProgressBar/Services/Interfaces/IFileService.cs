using System.IO;
using System.Threading.Tasks;

namespace DownloadProgressBar.Services.Interfaces
{
    public interface IFileService
    {
        Task<string> SaveFileAsync(string fileName, Stream content);
    }
}