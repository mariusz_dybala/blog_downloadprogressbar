using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using DownloadProgressBar.Utils;

namespace DownloadProgressBar.Services.Interfaces
{
    public interface IFileDownloader
    {
        Task<MemoryStream> DownloadFileAsync(string urlToDownload, Action<FileProgress> progressNotification,
            CancellationToken cancellationToken);
    }
}