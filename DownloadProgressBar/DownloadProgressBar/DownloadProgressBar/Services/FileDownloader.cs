using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using DownloadProgressBar.Services.Interfaces;
using DownloadProgressBar.Utils;

namespace DownloadProgressBar.Services
{
    public class FileDownloader : IFileDownloader
    {
        public async Task<MemoryStream> DownloadFileAsync(string urlToDownload,
            Action<FileProgress> progressNotification,
            CancellationToken cancellationToken)
        {
            try
            {
                using var client = new HttpClient();
                using HttpResponseMessage response = await client.GetAsync(urlToDownload,
                    HttpCompletionOption.ResponseHeadersRead, cancellationToken);
                using HttpContent content = response.Content;
                using Stream stream = await content.ReadAsStreamAsync();

                var contentLength = GetContentLength(content.Headers);
                return await ReadResponseStreamAsync(stream, progressNotification, contentLength, cancellationToken);
            }
            catch
            {
                return null;
            }
        }

        private string GetContentLength(HttpContentHeaders headers)
        {
            var contentLength = headers.First(x => x.Key.Equals("Content-Length"))
                .Value.First();

            return contentLength;
        }

        private static async Task<MemoryStream> ReadResponseStreamAsync(Stream stream,
            Action<FileProgress> progressNotification, string contentLength, CancellationToken cancellationToken)
        {
            var downloadingFileInfo = new FileProgress(int.Parse(contentLength));

            try
            {
                var memoryStream = new MemoryStream();
                var buffer = new byte[4096];

                for (;;)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    int bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length);
                    await Task.Delay(20);
                    if (bytesRead == 0)
                    {
                        await Task.CompletedTask;
                        break;
                    }

                    var dataFromBuffer = new byte[bytesRead];
                    buffer.ToList().CopyTo(0, dataFromBuffer, 0, bytesRead);
                    await memoryStream.WriteAsync(dataFromBuffer, 0, dataFromBuffer.Length);

                    downloadingFileInfo.ReadBytes += bytesRead;

                    progressNotification.Invoke(downloadingFileInfo);
                }

                downloadingFileInfo.IsFinished = true;
                progressNotification.Invoke(downloadingFileInfo);
           
                memoryStream.Position = 0;
                return memoryStream;
            }
            catch (OperationCanceledException operationCanceled)
            {   
                progressNotification.Invoke(downloadingFileInfo.Cancel());
                return null;
            }
        }
    }
}